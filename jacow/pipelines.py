# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import re
import hashlib
from scrapy.exceptions import DropItem
import logging
import spacy
# python -m spacy download en_core_web_sm


class ComferencePipeline(object):
    year_pattern = re.compile(r'(\d{2})')

    def process_item(self, item, spider):
        if item.get("name"):
            match = self.year_pattern.search(item.get("name"))
            if match is not None:
                y = match.group()
                if int(y) >= 0 and int(y) <= 30:
                    item["year"] = int("20" + y)
                else:
                    item["year"] = int("19" + y)
                return item
        if item.get("year"):
            item["year"] = max(set(item["year"]),
                               key=item["year"].count)
        return item


class ValidatePaperPipeline(object):
    def process_item(self, item, spider):
        if item.get("conf") is None or \
           item.get("name") is None or \
           item.get("year") is None or \
           item.get("conf_link") is None or \
           item.get("info_link") is None or \
           item.get("title") is None or \
           item.get("paper_id") is None or \
           item.get("authors") is None:
            spider.log("paper dropped", logging.WARNING)
            raise DropItem("Partially filled paper entry")
        return item


processed_papers = []


class PaperDuplicatesPipeline(object):
    def process_item(self, item, spider):
        m = hashlib.md5()
        m.update(item.get("conf", "").encode())
        m.update(item.get("name", "").encode())
        m.update(item.get("title", "").encode())
        if m.hexdigest() not in processed_papers:
            processed_papers.append(m.hexdigest())
            return item
        else:
            raise DropItem(f"Duplicate paper")


class ParseBibTeXPipeline(object):
    title_id_pattern = re.compile(r'PAPER\s+=\s+{(.*?)},')
    author_pattern = re.compile(r'AUTHOR\s+=\s+{(.*?)},')
    author_truncated_pattern = re.compile(r'AUTHOR\s+=\s+{(.*?OTHER.*?)},')
    keywords_pattern = re.compile(r'KEYWORDS\s+=\s+{(.*?)},')
    year_pattern = re.compile(r'YEAR\s+=\s+{(\d{4})},')
    title_pattern = re.compile(r'\s+TITLE\s+=\s+{(.+?)},')

    def process_item(self, item, spider):
        bibtex = item.get("bibtex")
        if bibtex is not None:
            bibtex = bibtex.upper()
            bibtex = bibtex.replace("\r\n", "")
            bibtex = bibtex.replace("\n", "")
            title_ip = self.title_id_pattern.findall(bibtex)
            if len(title_ip) > 0:
                item["paper_id"] = title_ip[0]
            authors = self.author_pattern.findall(bibtex)
            if len(authors) > 0:
                truncated = self.author_truncated_pattern.findall(bibtex)
                if len(truncated) == 0:
                    authors = authors[0]
                    authors = authors.replace("{", "")
                    authors = authors.replace("}", "")
                    authors = authors.split("AND")
                    authors = list(map(str.strip, authors))
                    item["authors"] = authors
            keywords = self.keywords_pattern.findall(bibtex)
            if len(keywords) > 0:
                keywords = keywords[0].split(",")
                item["keywords"] = list(map(str.strip, keywords))
            year = self.year_pattern.findall(bibtex)
            if len(year) > 0:
                item["year"] = year[0]
            title = self.title_pattern.findall(bibtex)
            if len(title) > 0:
                title = title[0]
                title = title.replace("{", "")
                title = title.replace("}", "")
                item["title"] = title.strip()
        return item


class AddKeywordsPipeline(object):
    nlp = spacy.load("en_core_web_sm")

    def process_item(self, item, spider):
        title = item.get("title")
        if title is not None:
            doc = self.nlp(title)
            keywords = item.get("keywords", [])
            keywords.extend([t.lemma_.upper() for t in doc if
                             t.pos_ == "NOUN"]) # or t.pos_ == "PROPN"
            item["keywords"] = set(keywords)
        return item
