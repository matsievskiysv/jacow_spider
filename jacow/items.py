# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
import scrapy.loader as loader
import scrapy.loader.processors as processors


class ConferenceItem(scrapy.Item):
    conf = scrapy.Field()
    name = scrapy.Field()
    year = scrapy.Field()
    authors_link = scrapy.Field()
    author_index_link = scrapy.Field()
    conf_link = scrapy.Field()


class ConferenceLoader(loader.ItemLoader):
    default_output_processor = processors.TakeFirst()
    conf_in = processors.MapCompose(str.strip, str.upper)
    name_in = processors.MapCompose(str.strip, str.upper)
    year_in = processors.MapCompose(int, lambda x: x if x > 1900 and
                                    x < 2030 else None)
    year_out = processors.Identity()


class PaperItem(scrapy.Item):
    conf = scrapy.Field()
    name = scrapy.Field()
    year = scrapy.Field()
    conf_link = scrapy.Field()
    info_link = scrapy.Field()
    title = scrapy.Field()
    pdf_link = scrapy.Field()
    paper_id = scrapy.Field()
    authors = scrapy.Field()
    bibtex = scrapy.Field()
    keywords = scrapy.Field()


class PaperLoader(loader.ItemLoader):
    default_output_processor = processors.TakeFirst()
    authors_in = processors.MapCompose(lambda x: x.split(","),
                                       str.strip, str.upper,
                                       lambda x: x.replace("\u00a0", " "))
    authors_out = processors.MapCompose(
        lambda x: None if x is None or x == "" else x)
    title_in = processors.MapCompose(str.strip, str.upper)
