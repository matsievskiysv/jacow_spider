# -*- coding: utf-8 -*-
import scrapy
import jsonlines
from ..items import (ConferenceItem,
                     PaperLoader,
                     PaperItem)


class PapersSpider(scrapy.Spider):
    name = 'papers'
    allowed_domains = ["accelconf.web.cern.ch"]
    filter_year = None
    filter_conf = None
    filter_name = None
    custom_settings = {
        'ITEM_PIPELINES': {
            'jacow.pipelines.ValidatePaperPipeline': 10,
            'jacow.pipelines.PaperDuplicatesPipeline': 50,
            'jacow.pipelines.ParseBibTeXPipeline': 60,
            'jacow.pipelines.AddKeywordsPipeline': 70,
        }
    }

    def start_requests(self):
        with jsonlines.open("conferences.jsonlines", "r") as f:
            for l in f:
                item = ConferenceItem(l)
                if item.get("authors_link"):
                    if item.get("year") is not None and \
                       self.filter_year is not None and \
                       item.get("year") != self.filter_year:
                        continue
                    if item.get("conf") is not None and \
                       self.filter_conf is not None and \
                       item.get("conf") != self.filter_conf:
                        continue
                    if item.get("name") is not None and \
                       self.filter_name is not None and \
                       item.get("name") != self.filter_name:
                        continue
                    yield scrapy.Request(url=item.get("authors_link"),
                                         callback=self.parse,
                                         cb_kwargs={"item": item})

    def parse(self, response, item):
        for a in response.css("a[href]"):
            yield response.follow(a,
                                  callback=self.author,
                                  cb_kwargs={"item": item})

    def author(self, response, item):
        ldr = None
        for s in response.css("tr"):
            if s.css('[class*="paptitle"]'):
                # new paper
                if ldr is not None:
                    yield ldr.load_item()
                ldr = PaperLoader(item=PaperItem(),
                                  selector=response)
                ldr.add_value("conf", item.get("conf"))
                ldr.add_value("name", item.get("name"))
                ldr.add_value("year", item.get("year"))
                ldr.add_value("conf_link", item.get("conf_link"))
                ldr.add_value("info_link", response.url)
                ldr.add_value("title", s.css('[class*="title"]::text').get())
                if s.css('a[href][target="pdf"]'):
                    ldr.add_value(
                        "pdf_link",
                        response.urljoin(
                            s.css('a[href][target="pdf"]::attr(href)').get()))
                    ldr.add_value("paper_id",
                                  s.css('a[href][target="pdf"]::text').get())
            if s.css('[class*=author]'):
                if ldr is None:
                    continue
                ldr.add_value("authors",
                              s.css('[class*=author]::text').getall())
            for a in s.css("a[href]"):
                if a.css("a::text").re("(?i).*bibtex.*"):
                    yield response.follow(a,
                                          callback=self.bibtex,
                                          cb_kwargs={"loader": ldr})
                    ldr = None
                    continue
        if ldr is not None:
            yield ldr.load_item()

    def bibtex(self, response, loader):
        loader.add_value("bibtex", response.css("pre::text").get())
        yield loader.load_item()
