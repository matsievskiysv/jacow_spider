# -*- coding: utf-8 -*-
import scrapy
from w3lib.html import remove_tags
from scrapy.utils.misc import extract_regex
from ..items import ConferenceLoader, ConferenceItem


class ConfSpider(scrapy.Spider):
    name = "conferences"
    allowed_domains = ['jacow.com',
                       'accelconf.web.cern.ch']
    start_urls = ["http://jacow.org/Main/Proceedings"]
    custom_settings = {
        'ITEM_PIPELINES': {
            'jacow.pipelines.ComferencePipeline': 400
        }
    }

    def parse(self, response):
        for conf in response.xpath('//tr[@class="conferences"]'):
            confname = conf.css("th::text").get()
            for a in conf.css("a"):
                loader = ConferenceLoader(item=ConferenceItem(),
                                          selector=a)
                loader.add_value("conf", confname)
                loader.add_css("name", "a::text")
                yield response.follow(a,
                                      callback=self.conf_page,
                                      cb_kwargs={"loader": loader})

    def conf_page(self, response, loader):
        loader.add_value("conf_link", response.url)
        loader.add_value("year",
                         extract_regex(r"\d{4}",
                                       remove_tags(response.text)))
        a = list(filter(lambda x: len(
            x.css("a").re(r"(?i).*author.*")) > 0,
                        response.css('a[href]')))
        if len(a) > 0:
            yield response.follow(a[0],
                                  callback=self.frame_page,
                                  cb_kwargs={"loader": loader})
        else:
            yield loader.load_item()

    def frame_page(self, response, loader):
        a = response.css('frame::attr(src)').getall()
        if len(a) == 3:
            yield response.follow(a[1],
                                  callback=self.authors_page,
                                  cb_kwargs={"loader": loader})
        else:
            loader.add_value("author_index_link", response.url)
            yield loader.load_item()

    def authors_page(self, response, loader):
        loader.add_value("authors_link", response.url)
        yield loader.load_item()
