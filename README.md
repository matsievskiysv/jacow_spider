In order to get paper metadata, run
```bash
> scrapy crawl conferences -L ERROR -o conferences.jsonlines
> scrapy crawl papers -L ERROR -o papers.jsonlines
```
